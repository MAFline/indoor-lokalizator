package cz.uhk.fim.indoorlokalizator.services;

import android.app.Service;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import cz.uhk.fim.indoorlokalizator.core.scanners.BleScanner;
import cz.uhk.fim.indoorlokalizator.core.scanners.Scanner;
import cz.uhk.fim.indoorlokalizator.fragments.ScanningFragment;
import cz.uhk.fim.indoorlokalizator.models.MapManager;

/**
 * Abstract service working with new Thread and scanning data.
 */
public abstract class ScanningService extends Service {
    public static final String TAG = "ScanningService";

    private Scanner scanner;
    private Thread thread;
    private volatile boolean isServiceRunning = false;
    private int broadcastTime;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        scanner = new BleScanner(bluetoothManager.getAdapter());

        broadcastTime = intent.getIntExtra(ScanningFragment.SEEK_VALUE_TAG, 1) * 1000;

        isServiceRunning = true;
        scanner.startScan();

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isServiceRunning) {
                    try {
                        Thread.sleep(broadcastTime);
                    } catch (InterruptedException e) {
                        return;
                    }

                    processData();
                }
            }
        });
        thread.start();

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        isServiceRunning = false;
        thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException e) {
        }

        scanner.stopScan();

        // after stop service we don't need measured data yet
        MapManager.getInstance().deleteMeasuredRssis();

        super.onDestroy();
    }

    /**
     * Abstract method to make calculating with measured data.
     */
    protected abstract void processData();
}
