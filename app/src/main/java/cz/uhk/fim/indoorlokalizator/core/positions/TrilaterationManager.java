package cz.uhk.fim.indoorlokalizator.core.positions;

import android.graphics.PointF;
import android.util.Log;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Beacon;

import static org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer.Optimum;

/**
 * Class using trilateration method to compute position.
 */
public class TrilaterationManager implements LocalizationManager {
    public static final String TAG = "LocalizationManager";
    public static final double ENVIRONMENT_ATTENUATION = 3.7;

    @Override
    public PointF getPosition(List<Beacon> beacons) {
        List<Beacon> distanceBeacons = new ArrayList<>();
        for (Beacon beacon : beacons) {
            beacon.setDistance(getDistanceInMeters(beacon));
            if (beacon.getDistance() > 0) {
                distanceBeacons.add(beacon);
            }
        }

        if (distanceBeacons.size() < 3) {
            Log.d(TAG, "pocet distanci: " + distanceBeacons.size());
            // too few distances, minimum 3
            return null;
        }
        
        double[][] positions = new double[distanceBeacons.size()][2];
        double[] distances = new double[distanceBeacons.size()];
        for (int i = 0; i < distanceBeacons.size(); i++) {
            Beacon b = distanceBeacons.get(i);
            positions[i][0] = b.getPosition_x();
            positions[i][1] = b.getPosition_y();
            distances[i] = b.getDistance();
        }

        NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
        Optimum optimum = solver.solve();
        return new PointF((float) optimum.getPoint().getEntry(0), (float) optimum.getPoint().getEntry(1));
    }

    @Override
    public double getDistanceInMeters(Beacon beacon) {
        if (beacon.getRssiAt1m() == null || beacon.getRssis().isEmpty()) {
            return -1.0;
        }

        return distanceInMeters(beacon.getMedianRssi(), beacon.getRssiAt1m());
    }

    private double distanceInMeters(int rssi, int txPower) {
        return Math.pow(10d, ((double) txPower - rssi) / ((double) 10 * ENVIRONMENT_ATTENUATION));
    }

}
