package cz.uhk.fim.indoorlokalizator.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Beacon;

/**
 * Util class working with canvas.
 */
public class DrawingUtils {
    public static final String TAG = "DrawingUtils";

    /**
     * Draw single circle to Bitmap at given position.
     * @param bitmap Background.
     * @param p Point where to draw.
     * @param size Size of the circle.
     * @param color Color of the circle.
     * @param scale Picture scale to correct coordinates.
     * @return
     */
    public static Bitmap drawPositionToBitmap(Bitmap bitmap, PointF p, int size, int color, double scale) {
        Bitmap output = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawCircle(p.x * (float) scale, p.y * (float) scale, size, paint);
        return output;
    }

    /**
     * Draw all beacons as circle to Bitmap.
     * @param bitmap Background.
     * @param beacons List with beacons.
     * @param size Size of the circle.
     * @param color Color of the circle.
     * @param scale Picture scale to correct coordinates.
     * @return
     */
    public static Bitmap drawBeaconsToBitmap(Bitmap bitmap, List<Beacon> beacons, int size, int color, double scale) {
        Bitmap output = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);

        for (Beacon beacon : beacons) {
            canvas.drawCircle((float) beacon.getPosition_x() * (float) scale, (float) beacon.getPosition_y() * (float) scale, size, paint);
        }

        return output;
    }
}
