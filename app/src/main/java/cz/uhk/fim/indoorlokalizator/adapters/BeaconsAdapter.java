package cz.uhk.fim.indoorlokalizator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.models.Beacon;

/**
 * Adapter with list of Beacons.
 */
public class BeaconsAdapter extends BaseAdapter {
    public static final String TAG = "BeaconsAdapter";

    private List<Beacon> beacons;
    private LayoutInflater inflater;

    public BeaconsAdapter(Context context, List<Beacon> beacons) {
        this.beacons = beacons;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public Object getItem(int position) {
        return beacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.list_beacon, null);

        TextView beaconUuid = (TextView) convertView.findViewById(R.id.beacon_uuid);
        TextView beaconMajor = (TextView) convertView.findViewById(R.id.beacon_major);
        TextView beaconMinor = (TextView) convertView.findViewById(R.id.beacon_minor);
        TextView beaconDistance = (TextView) convertView.findViewById(R.id.beacon_distance);

        Beacon beacon = beacons.get(position);

        beaconUuid.setText(beacon.getUuid());
        beaconMajor.setText(String.valueOf(beacon.getMajor()));
        beaconMinor.setText(String.valueOf(beacon.getMinor()));

        if (beacon.getDistance() < 0) {
            beaconDistance.setText(R.string.beacon_distance_default);
        } else {
            DecimalFormat df = new DecimalFormat("#.##");
            beaconDistance.setText("~ " + df.format(beacon.getDistance()) + " m");
        }

        return convertView;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }
}
