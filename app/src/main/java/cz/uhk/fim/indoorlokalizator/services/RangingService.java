package cz.uhk.fim.indoorlokalizator.services;

import android.content.Intent;

import cz.uhk.fim.indoorlokalizator.core.positions.LocalizationManager;
import cz.uhk.fim.indoorlokalizator.core.positions.TrilaterationManager;
import cz.uhk.fim.indoorlokalizator.models.Beacon;
import cz.uhk.fim.indoorlokalizator.models.MapManager;

/**
 * Service with calculating distances to beacons.
 */
public class RangingService extends ScanningService {
    public static final String TAG = "RangingService";
    public static final String BROADCAST_DISTANCES = "cz.uhk.fim.indoorlokalizator.services.RangingService.broadcast_action.distances";

    private LocalizationManager localizationManager;

    @Override
    public void onCreate() {
        localizationManager = new TrilaterationManager();
    }

    /**
     * Calculate distances for each beacon and send broacast to Activity.
     */
    @Override
    protected void processData() {
        for (Beacon beacon : MapManager.getInstance().getMap().getBeacons()) {
            beacon.setDistance(localizationManager.getDistanceInMeters(beacon));
            beacon.getRssis().clear();
        }

        final Intent i = new Intent();
        i.setAction(BROADCAST_DISTANCES);
        sendBroadcast(i);
    }
}
