package cz.uhk.fim.indoorlokalizator.fragments;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.adapters.MapsAdapter;
import cz.uhk.fim.indoorlokalizator.core.dblayer.DatabaseManager;
import cz.uhk.fim.indoorlokalizator.core.dblayer.SQLiteManager;
import cz.uhk.fim.indoorlokalizator.models.Map;
import cz.uhk.fim.indoorlokalizator.models.MapManager;
import cz.uhk.fim.indoorlokalizator.utils.DrawingUtils;
import cz.uhk.fim.indoorlokalizator.utils.JsonUtils;

/**
 * Fragment with Map list.
 * In this class user choose Map which is added to singleton object.
 */
public class MapsFragment extends ListFragment implements AdapterView.OnItemClickListener {
    public static final String TAG = "MapsFragment";

    private MapsAdapter mapsAdapter;
    private DatabaseManager databaseManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseManager = new SQLiteManager(getActivity());
        mapsAdapter = new MapsAdapter(getActivity(), findAllMaps());
        setListAdapter(mapsAdapter);
        getListView().setOnItemClickListener(this);
    }

    /**
     * Find all maps. First from raw source, others from database.
     * @return List of all Maps in application.
     */
    private List<Map> findAllMaps() {
        List<Map> maps = new ArrayList<>();
        maps.add(JsonUtils.createMapFromJson(JsonUtils.getStringFromInputStream(getResources().openRawResource(R.raw.luzna))));
        maps.addAll(databaseManager.getAllMaps());
        return maps;
    }

    @Override
    public void onStop() {
        databaseManager.closeConnection();
        super.onStop();
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, long id) {
        // dialog window with thumbnail and possibility to choose Map
        final Map map = (Map) getListAdapter().getItem(position);
        AlertDialog.Builder mapDialog = new AlertDialog.Builder(getActivity());
        mapDialog.setTitle(map.getName());
        ImageView mapImage = new ImageView(getActivity());

        Bitmap bitmap = DrawingUtils.drawBeaconsToBitmap(
                map.getPlatform().getPicture(),
                map.getBeacons(),
                5,
                Color.parseColor(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_color_beacon", null)),
                map.getPlatform().getScale()
        );

        mapImage.setImageBitmap(bitmap);
        mapDialog.setView(mapImage);
        mapDialog.setPositiveButton(R.string.map_dialog_choose, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < parent.getCount(); i++) {
                    parent.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }

                mapsAdapter.selectMap(view, position);
            }
        });
        mapDialog.setNegativeButton(R.string.map_dialog_back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // just close dialog
            }
        });

        // option to delete Map if is deletable
        if (map.isDeletable()) {
            mapDialog.setNeutralButton(R.string.map_dialog_delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (MapManager.getInstance().getMap().equals(map)) {
                        MapManager.getInstance().setMap(null);
                    }
                    databaseManager.deleteMap(map);

                    // after delete update adapter
                    mapsAdapter.setMaps(findAllMaps());
                    mapsAdapter.notifyDataSetChanged();
                }
            });
        }

        mapDialog.show();
    }

}
