package cz.uhk.fim.indoorlokalizator.adapters;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.models.Map;
import cz.uhk.fim.indoorlokalizator.models.MapManager;

/**
 * Adapter with list of Maps.
 */
public class MapsAdapter extends BaseAdapter {
    public static final String TAG = "MapsAdapter";
    private List<Map> maps;
    private LayoutInflater inflater;
    private Context context;

    public MapsAdapter(Context context, List<Map> maps) {
        this.maps = maps;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return maps.size();
    }

    @Override
    public Object getItem(int position) {
        return maps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.list_map, null);

        ImageView mapThumbnail = (ImageView) convertView.findViewById(R.id.map_thumbnail);
        TextView mapName = (TextView) convertView.findViewById(R.id.map_name);
        TextView mapSize = (TextView) convertView.findViewById(R.id.map_size);
        TextView mapBeaconCount = (TextView) convertView.findViewById(R.id.map_beacons_count);

        Map map = maps.get(position);

        mapThumbnail.setImageBitmap(ThumbnailUtils.extractThumbnail(map.getPlatform().getPicture(), 96, 96));
        mapName.setText(map.getName());
        mapSize.setText(map.getPlatform().getWidth() + " ¤ " + map.getPlatform().getHeight() + " m");
        mapBeaconCount.setText(String.valueOf(map.getBeacons().size()));

        // decide about map selection
        Map actualMap = MapManager.getInstance().getMap();
        if (actualMap == null) {
            selectMap(convertView, 0);
        } else {
            if (actualMap.equals(maps.get(position))) {
                selectMap(convertView, position);
            }
        }

        return convertView;
    }

    /**
     * Set map to singleton class and set background in ListView.
     * @param view
     * @param position
     */
    public void selectMap(View view, int position) {
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.fragment_maps_chosen));
        try {
            MapManager.getInstance().setMap(maps.get(position));
        } catch (IndexOutOfBoundsException e) {
            // shouldn't happen
            Log.e(TAG, "Bad map index.", e);
        }
    }

    public void setMaps(List<Map> maps) {
        this.maps = maps;
    }
}
