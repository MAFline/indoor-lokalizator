package cz.uhk.fim.indoorlokalizator.core.positions;

import android.graphics.PointF;

import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Beacon;

/**
 * Intefrace with localization methods.
 */
public interface LocalizationManager {
    public PointF getPosition(List<Beacon> beacons);
    public double getDistanceInMeters(Beacon beacon);
}
