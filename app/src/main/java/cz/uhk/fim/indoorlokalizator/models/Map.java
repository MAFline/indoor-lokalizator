package cz.uhk.fim.indoorlokalizator.models;

import java.util.List;

/**
 * Model class representing Map data.
 */
public class Map {
    private String name;
    private Platform platform;
    private List<Beacon> beacons;
    private boolean isDeletable;

    public Map(String name, Platform platform, List<Beacon> beacons) {
        this(name, platform, beacons, false);
    }

    public Map(String name, Platform platform, List<Beacon> beacons, boolean isDeletable) {
        this.name = name;
        this.platform = platform;
        this.beacons = beacons;
        this.isDeletable = isDeletable;
    }

    public Map() {
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeletable() {
        return isDeletable;
    }

    public void setDeletable(boolean deletable) {
        isDeletable = deletable;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
        }

        if (!(o instanceof Map)) {
            return false;
        }

        Map map = (Map) o;

        if (!this.name.equals(map.getName())) {
            return false;
        }
        if (!this.platform.equals(map.getPlatform())) {
            return false;
        }
        if (!this.beacons.equals(map.getBeacons())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode() * platform.hashCode() * beacons.hashCode();
    }

}
