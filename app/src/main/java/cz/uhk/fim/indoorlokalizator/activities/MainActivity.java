package cz.uhk.fim.indoorlokalizator.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.fragments.LocalizationFragment;
import cz.uhk.fim.indoorlokalizator.fragments.MapsFragment;
import cz.uhk.fim.indoorlokalizator.fragments.RangingFragment;

/**
 * Main Activity. Starting application point with DrawerLayout.
 */
public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    public static final int REQUEST_CODE_MAP_INSERTION = 4;
    public static final int REQUEST_CODE_SETTINGS = 2;
    public static final int REQUEST_CODE_ABOUT = 3;
    public static final int PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 1;

    private String tabMaps;
    private String tabLocalization;
    private String tabRanging;
    private TabLayout tabLayout;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        tabMaps = getResources().getString(R.string.tab_maps);
        tabLocalization = getResources().getString(R.string.tab_localization);
        tabRanging = getResources().getString(R.string.tab_ranging);

        setSupportActionBar(toolbar);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeFragment(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                drawerLayout.closeDrawers();
                int id = item.getItemId();

                switch (id) {
                    case R.id.navigation_insert_map:
                        // check permissions for read external storage
                        // with denied permissions for api 23+ activity won't start
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
                        } else {
                            startInsertMapActivity();
                        }
                        break;
                    case R.id.navigation_settings:
                        Log.d(TAG, "startuju aktivitu Settings");
                        Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivityForResult(settingsIntent, REQUEST_CODE_SETTINGS);
                        break;
                    case R.id.navigation_about:
                        Intent aboutIntent = new Intent(MainActivity.this, AboutActivity.class);
                        startActivityForResult(aboutIntent, REQUEST_CODE_ABOUT);
                        break;
                }

                return true;
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.toolbar_drawer_open, R.string.toolbar_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // set fragment with maps as default
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MapsFragment mapsFragment = new MapsFragment();
        ft.add(R.id.main_frag_container, mapsFragment);
        ft.commit();
    }

    /**
     * Starting MapInsertionActivity in single method for multiuse.
     */
    private void startInsertMapActivity() {
        Intent insertionIntent = new Intent(this, MapInsertionActivity.class);
        startActivityForResult(insertionIntent, REQUEST_CODE_MAP_INSERTION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startInsertMapActivity();
                } else {
                    navigationView.setCheckedItem(R.id.navigation_home);
                }
                return;
        }
    }

    /**
     * Replacing fragment when TabLayout is changed.
     * @param tab Tab name.
     */
    private void changeFragment(String tab) {
        Fragment fragment = null;
        if (tab.equals(tabMaps)) {
            fragment = new MapsFragment();
        } else if (tab.equals(tabLocalization)) {
            fragment = new LocalizationFragment();
        } else if (tab.equals(tabRanging)) {
            fragment = new RangingFragment();
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_frag_container, fragment);
            ft.commit();
        } else {
            Log.d(TAG, "No fragment to replace!");
        }

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // set navigationview to MainActivity when other is close.
        // not optimal.
        switch (requestCode) {
            case REQUEST_CODE_MAP_INSERTION:
                navigationView.setCheckedItem(R.id.navigation_home);
                break;
            case REQUEST_CODE_SETTINGS:
                navigationView.setCheckedItem(R.id.navigation_home);
                break;
            case REQUEST_CODE_ABOUT:
                navigationView.setCheckedItem(R.id.navigation_home);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}