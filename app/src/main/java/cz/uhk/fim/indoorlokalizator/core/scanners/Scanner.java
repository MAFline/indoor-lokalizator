package cz.uhk.fim.indoorlokalizator.core.scanners;

/**
 * Inteface for controlling scan.
 */
public interface Scanner {
    public void startScan();
    public void stopScan();
}
