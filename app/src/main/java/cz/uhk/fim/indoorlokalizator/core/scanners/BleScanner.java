package cz.uhk.fim.indoorlokalizator.core.scanners;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.MapManager;

/**
 * Class using BlueTooth Low Energy to scan iBeacon packets.
 */
public class BleScanner implements Scanner {
    private static final String TAG = "BleScanner";

    private int SDK_VERSION;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private ScanSettings scanSettings;
    private List<ScanFilter> scanFilter;

    // variables used according to API
    private BluetoothAdapter.LeScanCallback scanCallbackOld;
    private ScanCallback scanCallbackNew;

    public BleScanner(BluetoothAdapter bluetoothAdapter) {
        this.bluetoothAdapter = bluetoothAdapter;
        init();
    }

    private void init() {
        SDK_VERSION = Build.VERSION.SDK_INT;

        if (SDK_VERSION < 21) {
            scanCallbackOld = setScanCallbackOld();
        } else {
            scanCallbackNew = setScanCallbackNew();
            bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            scanFilter = new ArrayList<ScanFilter>();
        }
    }

    @Override
    public void startScan() {
        Log.d(TAG, "Starting scanning...");
        if (SDK_VERSION < 21) {
            bluetoothAdapter.startLeScan(scanCallbackOld);
        } else {
            bluetoothLeScanner.startScan(scanFilter, scanSettings, scanCallbackNew);
        }
    }

    @Override
    public void stopScan() {
        Log.d(TAG, "Stop scanning.");
        if (SDK_VERSION < 21) {
            bluetoothAdapter.stopLeScan(scanCallbackOld);
        } else {
            bluetoothLeScanner.stopScan(scanCallbackNew);
        }
    }

    /**
     * LeScanCallback for API level 21+
     */
    @TargetApi(21)
    private ScanCallback setScanCallbackNew() {
        return new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, android.bluetooth.le.ScanResult result) {
                synchronized (MapManager.getInstance().getMap()) {
                    analyzeScanResult(result.getRssi(), result.getScanRecord().getBytes());
                }
            }
        };
    }

    /**
     * LeScanCallback for api API <21
     */
    private BluetoothAdapter.LeScanCallback setScanCallbackOld() {
        return new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                analyzeScanResult(rssi, scanRecord);
            }
        };
    }

    /**
     * Parse packet data given by iBeacon.
     * @param rssi Received signal strength indicator.
     * @param scanResult Byte array of packet data.
     */
    private void analyzeScanResult(int rssi, byte[] scanResult) {
        Log.d(TAG, "Analyze result.");
        Log.d(TAG, "length of packet: " + scanResult.length);
        int startByte = 2;
        boolean patternFound = false;
        while (startByte <= 5) {
            if (((int) scanResult[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                    ((int) scanResult[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                patternFound = true;
                break;
            }
            startByte++;
        }

        if (patternFound) {
            // Recognize UUID
            byte[] uuidBytes = new byte[16];
            System.arraycopy(scanResult, startByte + 4, uuidBytes, 0, 16);
            String hexString = bytesToHex(uuidBytes);

            String uuid = hexString.substring(0, 8) + "-" +
                    hexString.substring(8, 12) + "-" +
                    hexString.substring(12, 16) + "-" +
                    hexString.substring(16, 20) + "-" +
                    hexString.substring(20, 32);

            final int major = (scanResult[startByte + 20] & 0xff) * 0x100 + (scanResult[startByte + 21] & 0xff);
            final int minor = (scanResult[startByte + 22] & 0xff) * 0x100 + (scanResult[startByte + 23] & 0xff);
            final int rssiAt1m = (scanResult[startByte + 24]);

            Log.d(TAG, "UUID: " + uuid);
            Log.d(TAG, "major: " + major);
            Log.d(TAG, "minor: " + minor);

            // Save packet data to singleton manager.
            MapManager.getInstance().addNewRssi(uuid, major, minor, rssiAt1m, rssi);
        }
    }

    // array for converting bytes to hex
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Helper method for converting bytes to hex.
     */
    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
