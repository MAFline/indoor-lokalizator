package cz.uhk.fim.indoorlokalizator.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Beacon;
import cz.uhk.fim.indoorlokalizator.models.Map;
import cz.uhk.fim.indoorlokalizator.models.Platform;

/**
 * Util class to Match JSON document with Map object.
 */
public class JsonUtils {
    public static final String TAG = "JsonUtils";

    /**
     * Create Map from JSON with false deletable attribute.
     * @param json JSON String.
     * @return Map object or null;
     */
    public static Map createMapFromJson(String json) {
        return createMapFromJson(json, false);
    }

    /**
     * Create Map from JSON String.
     * @param json JSON String.
     * @param deletable Flag if Map should be deletable or not.
     * @return Map object or null.
     */
    public static Map createMapFromJson(String json, boolean deletable) {
        try {
            JSONObject mapObject = new JSONObject(json);

            String name = mapObject.getString("name");
            double width = mapObject.getDouble("width");
            double height = mapObject.getDouble("height");
            JSONArray beaconsArray = mapObject.getJSONArray("beacons");

            List<Beacon> beacons = new ArrayList<>();
            for (int i = 0; i < beaconsArray.length(); i++) {
                JSONObject beaconObject = beaconsArray.getJSONObject(i);
                String uuid = beaconObject.getString("UUID");
                int major = beaconObject.getInt("major");
                int minor = beaconObject.getInt("minor");
                double position_x = beaconObject.getDouble("position_x");
                double position_y = beaconObject.getDouble("position_y");
                beacons.add(new Beacon(uuid, major, minor, position_x, position_y));
            }

            // decode Base64 String to Bitmpa
            byte[] image = Base64.decode(mapObject.getString("image"), Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);

            Platform platform = new Platform(bitmap, width, height);

            return new Map(name, platform, beacons, deletable);
        } catch (JSONException e) {
            Log.e(TAG, "Unable to create Map object from Json.", e);
            return null;
        }
    }

    /**
     * Converting InputStream to String.
     * @param is InputStream object.
     * @return String.
     */
    public static String getStringFromInputStream(InputStream is) {
        Writer writer = new StringWriter();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))){
            String line = null;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return writer.toString();
    }

    /**
     * Converting Map object to JSON document.
     * @param map
     * @return String with JSON document or null.
     */
    public static String convertMapToJsonString(Map map) {
        JSONObject mapObject = null;
        try {
            mapObject = new JSONObject();
            mapObject.put("name", map.getName());
            mapObject.put("width", map.getPlatform().getWidth());
            mapObject.put("height", map.getPlatform().getHeight());

            // if image string is missing (example map doesn't have Bitmap)
            if (map.getPlatform().getPicture() == null) {
                mapObject.put("image", "Base64 string");
            } else {
                mapObject.put("image", map.getPlatform().getPictureBase64String());
            }

            JSONArray beaconsArray = new JSONArray();
            for (Beacon beacon : map.getBeacons()) {
                JSONObject beaconObject = new JSONObject();
                beaconObject.put("UUID", beacon.getUuid());
                beaconObject.put("major", beacon.getMajor());
                beaconObject.put("minor", beacon.getMinor());
                beaconObject.put("position_x", beacon.getPosition_x());
                beaconObject.put("position_y", beacon.getPosition_y());
                beaconsArray.put(beaconObject);
            }
            mapObject.put("beacons", beaconsArray);

            return mapObject.toString(2);
        } catch (JSONException e) {
            Log.e(TAG, "Error while creating JSON from Map.");
        }
        return null;

    }
}
