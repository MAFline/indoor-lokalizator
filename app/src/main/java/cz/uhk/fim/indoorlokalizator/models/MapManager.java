package cz.uhk.fim.indoorlokalizator.models;

import java.util.Arrays;

/**
 * Singleton object with Map data forwarded across the application.
 */
public class MapManager {
    public static final String TAG = "MapManager";
    private static MapManager instance = null;
    private Map map;

    private MapManager() {
    }

    public static MapManager getInstance() {
        if (instance == null) {
            instance = new MapManager();
        }
        return instance;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * Clear collected RSSI values for beacons in map.
     */
    public void deleteMeasuredRssis() {
        for (Beacon beacon : map.getBeacons()) {
            beacon.getRssis().clear();
            beacon.setDistance(-1.0);
        }
    }

    /**
     * Identify iBeacon and set it new RSSI value.
     * @param uuid
     * @param major
     * @param minor
     * @param rssiAt1m
     * @param rssi
     */
    public void addNewRssi(String uuid, int major, int minor, int rssiAt1m, int rssi) {
        Beacon b = map.getBeacons().get(map.getBeacons().indexOf(new Beacon(uuid, major, minor)));
        if (b != null) {
            b.addRssi(rssi);
            if (b.getRssiAt1m() == null) {
                b.setRssiAt1m(rssiAt1m);
            }
        }
    }

    /**
     * Example map shown in example JSON to import.
     * @return Map with example data.
     */
    public static Map createExampleMap() {
        return new Map(
                "Map name",
                new Platform(null, 10.54, 8.4),
                Arrays.asList(
                        new Beacon("cb188070-8a36-11e6-ae22-56b6b6499611", 1000, 2000, 3.5, 2.1),
                        new Beacon("cb188070-8a36-11e6-ae22-56b6b6499611", 1001, 2001, 2.8, 8.1),
                        new Beacon("cb188070-8a36-11e6-ae22-56b6b6499611", 1002, 2002, 4.9, 7.0)
                ));
    }
}
