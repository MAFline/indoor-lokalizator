package cz.uhk.fim.indoorlokalizator.core.dblayer;

import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Map;

/**
 * Interface with database manipulation.
 */
public interface DatabaseManager {
    public void saveMap(Map map);
    public void deleteMap(Map map);
    public List<Map> getAllMaps();
    public void closeConnection();
}
