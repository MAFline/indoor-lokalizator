package cz.uhk.fim.indoorlokalizator.core.dblayer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.fim.indoorlokalizator.models.Map;
import cz.uhk.fim.indoorlokalizator.utils.JsonUtils;

/**
 * Managing SQLlite database.
 */
public class SQLiteManager extends SQLiteOpenHelper implements DatabaseManager {
    public static final String TAG = "SQLiteManager";
    public static final String DB_NAME = "IndoorLokalizator";
    public static final String MAPS_TABLE_NAME = "Maps";
    public static final String MAPS_COLUMN_MAP = "Map";

    public SQLiteManager(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + MAPS_TABLE_NAME + " (" + MAPS_COLUMN_MAP + " TEXT PRIMARY KEY)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MAPS_TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void saveMap(Map map) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MAPS_COLUMN_MAP, JsonUtils.convertMapToJsonString(map));
        db.insert(MAPS_TABLE_NAME, null, contentValues);
    }

    @Override
    public void deleteMap(Map map) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(MAPS_TABLE_NAME, MAPS_COLUMN_MAP + " = ?", new String[] {JsonUtils.convertMapToJsonString(map)});
    }

    @Override
    public List<Map> getAllMaps() {
        List<Map> maps = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + MAPS_TABLE_NAME, null);
        res.moveToFirst();
        if (!res.isAfterLast()) {
            maps.add(JsonUtils.createMapFromJson(res.getString(res.getColumnIndex(MAPS_COLUMN_MAP)), true));
        }
        return maps;
    }

    @Override
    public void closeConnection() {
        // SQLlite has autoclose.
    }
}
