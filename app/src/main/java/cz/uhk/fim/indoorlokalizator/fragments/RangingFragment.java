package cz.uhk.fim.indoorlokalizator.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.adapters.BeaconsAdapter;
import cz.uhk.fim.indoorlokalizator.models.Beacon;
import cz.uhk.fim.indoorlokalizator.models.MapManager;
import cz.uhk.fim.indoorlokalizator.services.RangingService;
import cz.uhk.fim.indoorlokalizator.utils.DrawingUtils;

/**
 * Fragment with list of Beacons and with scanning enabled estimoted distances.
 */
public class RangingFragment extends ScanningFragment implements AdapterView.OnItemClickListener {
    public static final String TAG = "RangingFragment";
    public static final int SEEK_MIN = 2;
    public static final int SEEK_MAX = 10;
    public static final int SEEK_INIT = 5;

    private ListView listView;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                    switch (state) {
                        case BluetoothAdapter.STATE_OFF:
                            RangingFragment.super.disableScan();
                            break;
                        default: break;
                    }
                    break;

                case RangingService.BROADCAST_DISTANCES:
                    BeaconsAdapter beaconsAdapter = (BeaconsAdapter) listView.getAdapter();
                    beaconsAdapter.setBeacons(MapManager.getInstance().getMap().getBeacons());
                    beaconsAdapter.notifyDataSetChanged();
                    break;

                default: break;
            }
        }
    };

    public RangingFragment() {
        super(R.layout.fragment_ranging, RangingService.class, SEEK_MIN, SEEK_MAX, SEEK_INIT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        listView = (ListView) view.findViewById(R.id.list_beacons);

        return view;
    }

    public void onStart() {
        super.onStart();

        // set beacons to adapter
        BeaconsAdapter beaconsAdapter = new BeaconsAdapter(getActivity(), MapManager.getInstance().getMap().getBeacons());
        listView.setAdapter(beaconsAdapter);
        listView.setOnItemClickListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(RangingService.BROADCAST_DISTANCES);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // dialog window with beacon position in map
        Beacon beacon = (Beacon) listView.getAdapter().getItem(position);
        AlertDialog.Builder ImageDialog = new AlertDialog.Builder(getActivity());
        ImageDialog.setTitle(R.string.beacon_image_title);
        ImageView showImage = new ImageView(getActivity());

        Bitmap bitmap = DrawingUtils.drawPositionToBitmap(
                MapManager.getInstance().getMap().getPlatform().getPicture(),
                new PointF((float) beacon.getPosition_x(), (float) beacon.getPosition_y()),
                5,
                Color.parseColor(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_color_beacon", null)),
                MapManager.getInstance().getMap().getPlatform().getScale()
        );

        showImage.setImageBitmap(bitmap);
        ImageDialog.setView(showImage);
        ImageDialog.setNegativeButton(R.string.beacon_image_button, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
            }
        });
        ImageDialog.show();
    }

}
