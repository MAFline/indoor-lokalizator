package cz.uhk.fim.indoorlokalizator.services;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;

import cz.uhk.fim.indoorlokalizator.core.positions.LocalizationManager;
import cz.uhk.fim.indoorlokalizator.core.positions.TrilaterationManager;
import cz.uhk.fim.indoorlokalizator.models.MapManager;

/**
 * Service with calculating position of device.
 */
public class LocalizationService extends ScanningService {
    public static final String TAG = "LocalizationService";
    public static final String BROADCAST_POSITION = "cz.uhk.fim.indoorlokalizator.service.LocalizationService.broadcast_action.position";
    public static final String BROADCAST_NOT_FOUND = "cz.uhk.fim.indoorlokalizator.service.LocalizationService.broadcast_action.not_found";
    public static final String EXTRA_X = "cz.uhk.fim.indoorlokalizator.service.LocalizationService.extras.x";
    public static final String EXTRA_Y = "cz.uhk.fim.indoorlokalizator.service.LocalizationService.extras.y";
    private static final int FAIL_LIMIT = 5;

    private int failCounter = 0;
    private LocalizationManager localizationManager;

    @Override
    public void onCreate() {
        localizationManager = new TrilaterationManager();
    }

    /**
     * Calculate position and send broadcast with new position to Activity.
     */
    @Override
    protected void processData() {
        PointF p = localizationManager.getPosition(MapManager.getInstance().getMap().getBeacons());
        if (p != null) {
            final Intent i = new Intent();
            i.setAction(BROADCAST_POSITION);
            Bundle b = new Bundle();
            b.putDouble(EXTRA_X, p.x);
            b.putDouble(EXTRA_Y, p.y);
            i.putExtras(b);
            sendBroadcast(i);

            failCounter = 0;
        } else {
            // undefined point increases fail counter
            failCounter++;
            if (failCounter > FAIL_LIMIT) {
                // too much null returns
                final Intent i = new Intent();
                i.setAction(BROADCAST_NOT_FOUND);
                sendBroadcast(i);
                failCounter = 0;
            }
        }

        // after computation delete measured RSSIs
        MapManager.getInstance().deleteMeasuredRssis();
    }

}
