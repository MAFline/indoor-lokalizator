package cz.uhk.fim.indoorlokalizator.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.commons.io.FileUtils;

import java.io.File;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.core.dblayer.DatabaseManager;
import cz.uhk.fim.indoorlokalizator.core.dblayer.SQLiteManager;
import cz.uhk.fim.indoorlokalizator.models.Map;
import cz.uhk.fim.indoorlokalizator.models.MapManager;
import cz.uhk.fim.indoorlokalizator.utils.FileChooser;
import cz.uhk.fim.indoorlokalizator.utils.JsonUtils;


/**
 * Fragment with new map import.
 */
public class MapInsertionFragment extends Fragment {
    public static final String TAG = "MapInsertionFragment";
    public static final int MAX_FILE_SIZE = 2000000;

    private FileChooser fileChooser;
    private Map exampleMap;
    private Button uploadButton;
    private Button chooseFileButton;
    private ImageButton exampleJsonButton;
    private LinearLayout fileUploadLayout;
    private TextView uploadTextView;
    private DatabaseManager databaseManager;
    private Map mapToUpload;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_insertion, container, false);

        uploadButton = (Button) view.findViewById(R.id.map_insertion_button_upload);
        chooseFileButton = (Button) view.findViewById(R.id.map_insertion_button_find);
        exampleJsonButton = (ImageButton) view.findViewById(R.id.map_insertion_button_example);
        uploadTextView = (TextView) view.findViewById(R.id.map_insertion_textview_file);
        fileUploadLayout = (LinearLayout) view.findViewById(R.id.map_insertion_layout_file_upload);

        fileUploadLayout.setVisibility(View.GONE);

        fileChooser = new FileChooser(getActivity()).setFileListener(new FileChooser.FileSelectedListener() {
            @Override
            public void fileSelected(File file) {
                fileUploadLayout.setVisibility(View.VISIBLE);
                try {
                    if (file.length() < MAX_FILE_SIZE) {
                        mapToUpload = JsonUtils.createMapFromJson(FileUtils.readFileToString(file, "UTF-8"), true);
                    }
                } catch (Exception e) {
                    // file is too big, probably not json
                    Log.e(TAG, "Unable to read file.", e);
                }

                if (mapToUpload != null) {
                    uploadButton.setVisibility(View.VISIBLE);
                    uploadTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));
                    uploadTextView.setText(file.getName());
                } else {
                    uploadButton.setVisibility(View.GONE);
                    uploadTextView.setText(R.string.map_insertion_textview_file_wrong);
                    uploadTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.map_insertion_textview_file_wrong));
                }

            }
        });
        fileChooser.setExtension("json");

        chooseFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileChooser.showDialog();
            }
        });
        exampleJsonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder jsonDialog = new AlertDialog.Builder(getActivity());
                jsonDialog.setTitle(R.string.map_insertion_dialog_example_title);
                ScrollView scrollView = new ScrollView(getActivity());
                scrollView.setLayoutParams(new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                scrollView.setPadding(5, 5, 5, 5);
                TextView textView = new TextView(getActivity());
                textView.setText(JsonUtils.convertMapToJsonString(exampleMap));
                scrollView.addView(textView);
                jsonDialog.setView(scrollView);
                jsonDialog.setPositiveButton(R.string.map_insertion_dialog_example_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // just close window
                    }
                });

                jsonDialog.show();
            }
        });
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseManager = new SQLiteManager(getActivity());
                databaseManager.saveMap(mapToUpload);
                getActivity().finish();
            }
        });

        exampleMap = MapManager.createExampleMap();

        return view;
    }

    @Override
    public void onStop() {
        if (databaseManager != null) {
            databaseManager.closeConnection();
        }
        super.onStop();
    }
}
