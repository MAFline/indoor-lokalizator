package cz.uhk.fim.indoorlokalizator.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Model class representing iBeacon data.
 */
public class Beacon {
    public static final String TAG = "Beacon";
    private double position_x;  // coordinates in meters
    private double position_y;  // coordinates in meters
    private String uuid;
    private int major;
    private int minor;
    private Integer rssiAt1m;
    private List<Integer> rssis;
    private double distance; // distance from receiver in meters

    public Beacon(String uuid, int major, int minor, double position_x, double position_y) {
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.position_x = position_x;
        this.position_y = position_y;
        rssis = new ArrayList<>();
        distance = -1.0;
    }

    public Beacon(String uuid, int major, int minor) {
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
    }

    public double getPosition_x() {
        return position_x;
    }

    public void setPosition_x(double position_x) {
        this.position_x = position_x;
    }

    public double getPosition_y() {
        return position_y;
    }

    public void setPosition_y(double position_y) {
        this.position_y = position_y;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public Integer getRssiAt1m() {
        return rssiAt1m;
    }

    public void setRssiAt1m(Integer rssiAt1m) {
        this.rssiAt1m = rssiAt1m;
    }

    public List<Integer> getRssis() {
        return rssis;
    }

    public void setRssis(List<Integer> rssis) {
        this.rssis = rssis;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public synchronized void addRssi(int rssi) {
        rssis.add(rssi);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Beacon)) return false;

        Beacon beacon = (Beacon) o;
        if (!this.uuid.equals(beacon.getUuid())) return false;
        if (this.major != beacon.major) return false;
        if (this.minor != beacon.minor) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode() * major * minor;
    }

    public synchronized int getMedianRssi() {
        if (rssiAt1m == null || rssis.isEmpty()) {
            // missing data
            return 0;
        }

        if (rssis.size() == 1) {
            return rssis.get(0);
        }

        Collections.sort(rssis);
        int median = 0;

        if (rssis.size() % 2 == 0) {
            median = (int) Math.round((double) (rssis.get(rssis.size()/2) + rssis.get(rssis.size()/2 - 1))/2);
        } else {
            median = Math.round(rssis.get(rssis.size()/2));
        }

        return median;
    }
}
