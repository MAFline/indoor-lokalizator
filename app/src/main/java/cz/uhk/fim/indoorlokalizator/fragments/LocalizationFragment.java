package cz.uhk.fim.indoorlokalizator.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.uhk.fim.indoorlokalizator.R;
import cz.uhk.fim.indoorlokalizator.models.MapManager;
import cz.uhk.fim.indoorlokalizator.services.LocalizationService;
import cz.uhk.fim.indoorlokalizator.utils.DrawingUtils;
import cz.uhk.fim.indoorlokalizator.views.ZoomableImageView;

/**
 * Fragment with Localization sectionl
 */
public class LocalizationFragment extends ScanningFragment {
    public static final String TAG = "LocalizationFragment";
    public static final int SEEK_MIN = 5;
    public static final int SEEK_MAX = 20;
    public static final int SEEK_INIT = 10;

    private ZoomableImageView platform;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                    switch (state) {
                        case BluetoothAdapter.STATE_OFF:
                            LocalizationFragment.super.disableScan();
                            break;
                        default: break;
                    }
                    break;

                case LocalizationService.BROADCAST_POSITION:
                    Bundle b = intent.getExtras();
                    PointF p = new PointF((float) b.getDouble(LocalizationService.EXTRA_X), (float) b.getDouble(LocalizationService.EXTRA_Y));
                    Bitmap bitmap = DrawingUtils.drawPositionToBitmap(platform.getOriginalBeaconsBitmap(), p, 15, Color.parseColor(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_color_position", null)), platform.getOriginalPlatform().getScale());
                    platform.setImageBitmap(bitmap);
                    break;

                default: break;
            }
        }
    };

    public LocalizationFragment() {
        super(R.layout.fragment_localization, LocalizationService.class, SEEK_MIN, SEEK_MAX, SEEK_INIT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        platform = (ZoomableImageView) view.findViewById(R.id.zoomable_view);
        setZoomableView();

        return view;
    }

    /**
     * Set bitmap for ImageView.
     */
    private void setZoomableView() {
        platform.setOriginalPlatform(MapManager.getInstance().getMap().getPlatform());

        // draw beacons to bitmap
        Bitmap beaconsBitmap = DrawingUtils.drawBeaconsToBitmap(platform.getOriginalPlatform().getPicture(), MapManager.getInstance().getMap().getBeacons(), 5, Color.parseColor(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_color_beacon", null)), platform.getOriginalPlatform().getScale());
        platform.setOriginalBeaconsBitmap(beaconsBitmap);
        platform.setImageBitmap(beaconsBitmap);
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(LocalizationService.BROADCAST_POSITION);
        intentFilter.addAction(LocalizationService.BROADCAST_NOT_FOUND);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

}