
package cz.uhk.fim.indoorlokalizator.models;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Model class with platform attributes.
 */
public class Platform {
    public static final String TAG = "Platform";

    private Bitmap picture;
    private double width;   // size in meters
    private double height;  // size in meters
    private double scale;

    public Platform(Bitmap picture, double width, double height) {
        if (picture != null) {
            this.picture = picture;
            this.scale = (double) picture.getWidth() / width;
        }
        this.width = width;
        this.height = height;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Platform)) return false;

        Platform platform = (Platform) o;
        if (!this.picture.sameAs(platform.getPicture())) {
            return false;
        }
        if (this.width != platform.getWidth()) {
            return false;
        }
        if (this.height != platform.getHeight()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (picture.hashCode() * width * height);
    }

    /**
     * Converting platform Bitmap to Base64 String.
     * @return Base64 String of Bitmap.
     */
    public String getPictureBase64String() {
        if (picture == null) {
            return null;
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
    }
}
