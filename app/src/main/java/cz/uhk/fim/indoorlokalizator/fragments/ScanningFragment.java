package cz.uhk.fim.indoorlokalizator.fragments;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import cz.uhk.fim.indoorlokalizator.R;

/**
 * Fragment as superclass for fragments using scanner.
 */
public abstract class ScanningFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {
    public static final String TAG = "ScanningFragment";
    public static final int REQUEST_ENABLE_BT = 1;
    public static final String SEEK_VALUE_TAG = "cz.uhk.fim.indoorlokalizator.fragments.ScanningFragment.seek_value";
    public static final int PERMISSION_REQUEST_ACCESS_COARSE_LOCATION = 2;

    private int layoudId;
    private Class service;
    private RelativeLayout panelScanLayout;
    private TextView panelScanText;
    private TextView panelSeekValue;
    private SeekBar panelSeekBar;
    private Button panelScanButton;
    private BluetoothAdapter bluetoothAdapter;
    private boolean isScanRunning = false;
    private int seekMin;
    private int seekMax;
    private int seekInit;

    public ScanningFragment(int layoutId, Class service, int seekMin, int seekMax, int seekInit) {
        this.layoudId = layoutId;
        this.service = service;
        this.seekMin = seekMin;
        this.seekMax = seekMax;
        this.seekInit = seekInit;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoudId, container, false);

        panelScanLayout = (RelativeLayout) view.findViewById(R.id.panel_scan_layout);
        panelScanText = (TextView) view.findViewById(R.id.panel_scan_text);
        panelScanButton = (Button) view.findViewById(R.id.panel_scan_button);
        panelSeekBar = (SeekBar) view.findViewById(R.id.panel_seek_bar);
        panelSeekValue = (TextView) view.findViewById(R.id.panel_seek_value);

        panelSeekBar.setMax(seekMax - seekMin);
        panelSeekBar.setProgress(Math.max(seekInit, seekMin));
        panelSeekBar.setOnSeekBarChangeListener(this);
        panelSeekValue.setText(String.valueOf(panelSeekBar.getProgress()));

        panelScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isScanRunning) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_ACCESS_COARSE_LOCATION);
                    } else {
                        enableScan();
                    }
                } else {
                    disableScan();
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableScan();
                }
        }
    }

    /**
     * Changes when scan should be enabled.
     */
    private void enableScan() {
        if (!bluetoothAdapter.isEnabled()) {
            Intent enablingBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enablingBluetoothIntent, REQUEST_ENABLE_BT);
        } else {
            panelScanLayout.setBackgroundResource(R.color.panel_scan_background_enable);
            panelScanText.setText(R.string.panel_scan_text_on);
            panelScanButton.setText(R.string.panel_scan_button_disable);
            isScanRunning = true;
            startService();
            Toast.makeText(getActivity(), getResources().getText(R.string.localization_started), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Starting service.
     */
    private void startService() {
        Intent i = new Intent(getActivity(), service);
        i.putExtra(SEEK_VALUE_TAG, panelSeekBar.getProgress() + seekMin);
        getActivity().startService(i);
    }

    /**
     * Stop service.
     */
    private void stopService() {
        getActivity().stopService(new Intent(getActivity(), service));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                enableScan();
            }
        }
    }

    /**
     * Changes when scan should be disabled.
     */
    protected void disableScan() {
        if (isScanRunning) {
            panelScanLayout.setBackgroundResource(R.color.panel_scan_background_disable);
            panelScanText.setText(R.string.panel_scan_text_off);
            panelScanButton.setText(R.string.panel_scan_button_enable);
            isScanRunning = false;
            stopService();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        bluetoothAdapter = ((BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableScan();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        panelSeekValue.setText(String.valueOf(progress + seekMin));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        stopService();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // start service again, when scan is enabled and seekbar released
        if (isScanRunning) {
            startService();
        }
    }
}
